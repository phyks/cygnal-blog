#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Phyks'
SITENAME = u'Cygnal'
SITEDESCRIPTION = 'Track and share issues in realtime on bike lanes!'
SITEURL = ''

PATH = 'content'
PLUGIN_PATHS = ['plugins']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'
LOCALE = ('en_US.UTF-8',)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

THEME = 'themes/custom'

STATIC_PATHS = ['images']

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.toc': {'anchorlink': True},
        'markdown.extensions.codehilite': {'css_class': 'highlight'}
    },
    'output_format': 'html5'
}

DEFAULT_DATE_FORMAT = '%d/%m/%Y'
